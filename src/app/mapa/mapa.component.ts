import { Component, OnInit } from '@angular/core';

import * as L from "leaflet";
import { ActivatedRoute } from '@angular/router';
import { MapaService } from '../_service/mapa.service';
import { NotifyService } from '../_service/notify.service';
import { CENTRO_MAPA } from '../_shared/constantes';

@Component({
  selector: 'app-mapa',
  templateUrl: './mapa.component.html',
  styleUrls: ['./mapa.component.css']
})
export class MapaComponent implements OnInit {

  id:number;
  alert:object = {};
  bomberos:Array<object> = [];
  comisarias:Array<object> = [];
  hospitales:Array<object> = [];


  constructor(
    private activatedRoute: ActivatedRoute,
    private mapaService:MapaService,
    private notifyService: NotifyService
  ) { }

  ngOnInit(): void {
    console.log('mapa');
    setTimeout(() => {
      this._initMap();
      this.loadInit();
    }, 1000);
  }

  private _initMap(){
    const CENTROMAPA = L.latLng(CENTRO_MAPA.latitud, CENTRO_MAPA.longitud);
    const mapa =  L.map("mapa", {
      zoomControl: false,
      center: CENTROMAPA,
      zoom: 4,
      minZoom: 2,
      maxZoom: 19,
      layers: [this.mapaService.mapasBase.Stadia_AlidadeSmoothDark]
    });
    L.control.zoom({ position: "topright" }).addTo(mapa);
    const controlLayer = L.control.layers(this.mapaService.mapasBase, this.mapaService.overlayers).addTo(mapa);
    L.control.scale().addTo(mapa);
    
    /*mapa.on('click', (e:any)=>{
      let marker = new L.marker(e.latlng, {draggable:'true'});
      marker.on('dragend', function(event){
        console.log(event);
      });
      mapa.addLayer(marker);
    });*/
    /*L.Control.searchChasqui({
      url: 'http://67.205.146.198:8083/chasqui/api/places/search-simple?text={text}',
      textPlaceholder:'Pacaya Samiria',
      propGeometry: 'geoJson',
      propertyName: 'nombreLugar',
      autoRemoveSarchTime: 15000
    }).addTo(mapa);*/
    this.mapaService.map = mapa;
    this.mapaService.controlLayer = controlLayer;

    
  }

  private async loadInit(){
    try {
      this.id = Number(this.activatedRoute.snapshot.paramMap.get("id"));
      if(this.id > 0){
        let response = await this.notifyService.getAlerta(this.id).toPromise();
        if(response['state'] == true){
          this.alert = response['data'][0];
          this.drawAlert();
        }
        let reponseBomberos = await this.notifyService.getBomberos(this.id).toPromise();
        if(reponseBomberos['state'] == true){
          this.bomberos = reponseBomberos['data']
          this.drawPlacesClose(this.bomberos, 'red', 'bombero');
        }

        let reponseComisarias = await this.notifyService.getComisarias(this.id).toPromise();
        if(reponseComisarias['state'] == true){
          this.comisarias = reponseComisarias['data']
          this.drawPlacesClose(this.comisarias, 'green', 'policia');
        }

        let reponseHospitales= await this.notifyService.getHospitales(this.id).toPromise();
        if(reponseHospitales['state'] == true){
          this.hospitales = reponseHospitales['data']
          this.drawPlacesClose(this.hospitales, 'orange', 'hospital');
        }
      }
    } catch (error) {
      
    }
  }

  private drawAlert(){
    let popUp = `<table>
                    <tr>
                      <td>Tipo alerta</td>
                      <td><b>${this.alert['type']}</b></td>
                    </tr>
                    <tr>
                      <td>Mensaje: </td>
                      <td><b>${this.alert['message']}</b></td>
                    </tr>
                    <tr>
                      <td>Celular: </td>
                      <td><b>${this.alert['userfrom']['phonenumber']}</b></td>
                    </tr>
                    <tr>
                      <td>Coordenadas: </td>
                      <td><b>${this.alert['latitud']} ${this.alert['longitud']}</b></td>
                    </tr>
                </table>`;
                var iconLaert = L.icon({
                  iconUrl: '../../assets/marker.png',
                  iconSize:     [20, 20], // size of the icon
              });
              var marker = L.marker([this.alert['longitud'], this.alert['latitud']],{icon: iconLaert}
            ).bindPopup(popUp).addTo(this.mapaService.map);
    var circle = L.circle([this.alert['longitud'], this.alert['latitud']], {
      color: 'blue',
      fillColor: '#f03',
      fillOpacity: 0.3,
      radius: 500
    }).addTo(this.mapaService.map);
    setTimeout(() => {
      this.mapaService.map.flyToBounds(circle.getBounds() , {maxZoom: 11});
    }, 1000);
    
    /*setTimeout(() => {
      this.mapaService.map.flyTo([this.alert['longitud'], this.alert['latitud']], 16);
    }, 2000);*/
    
  }

  private drawPlacesClose(places, color, icon){
    for(var i in places){
      var bomb = places[i];
      console.log('place', bomb['tipoLugar']);
      let popUp = `<table>
                    <tr>
                      <td>Dirección</td>
                      <td><b>${bomb['direccion']}</b></td>
                    </tr>
                    <tr>
                      <td>Nombre</td>
                      <td><b>${bomb['responsable']}</b></td>
                    </tr>
                    <tr>
                      <td>Responsable</td>
                      <td><b>${bomb['nombre']}</b></td>
                    </tr>
                    <tr>
                      <td>Teléfono</td>
                      <td><b>${bomb['telefono']}</b></td>
                    </tr>
                    <tr>
                      <td>Tipo</td>
                      <td><b>${bomb['tipoLugar']}</b></td>
                    </tr>
                  </table>`;
                  var _icon = L.icon({
                    iconUrl: '../../assets/icon-'+icon+'.png',
                    iconSize:     [20, 20], // size of the icon
                });
                  var _marker = L.marker([bomb['longitud'], bomb['latitud']],{icon: _icon}
                  ).bindPopup(popUp).addTo(this.mapaService.map);
                  /*var _circle = L.circle([bomb['longitud'], bomb['latitud']], {
                    color: color,
                    fillColor: 'black',
                    fillOpacity: 0.3,
                    radius: 180
                  }).bindPopup(popUp).addTo(this.mapaService.map);*/
    }
    
  }

}
