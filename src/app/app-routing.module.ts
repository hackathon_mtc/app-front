import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MapaComponent } from './mapa/mapa.component';


const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: '/alertas' },
  {path: 'alertas', component: MapaComponent},
  {path: 'alertas/:id', component: MapaComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
