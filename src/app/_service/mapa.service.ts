import { Injectable } from '@angular/core';
import * as L from "leaflet";

@Injectable({
  providedIn: 'root'
})
export class MapaService {

  public map: L.Map;
  public mapasBase: any;
  public overlayers:any={};
  public controlLayer: L.Control.Layers;

  constructor() { 

    const darkAttr = "&copy; <a href='https://stadiamaps.com/'>Stadia Maps</a>, &copy; <a href='https://openmaptiles.org/'>OpenMapTiles</a> &copy; <a href='http://openstreetmap.org'>OpenStreetMap</a> contributors";

    const osmAttr =
          "&copy; <a href='http://www.openstreetmap.org/copyright'>OpenStreetMap</a>, " +
          "Tiles courtesy of <a href='http://hot.openstreetmap.org/' target='_blank'>Humanitarian OpenStreetMap Team</a>";

        const esriAttr =
          "Tiles &copy; Esri &mdash; Esri, DeLorme, NAVTEQ, TomTom, Intermap, " +
          "iPC, USGS, FAO, NPS, NRCAN, GeoBase, Kadaster NL, Ordnance Survey, Esri Japan, METI, " +
          "Esri China (Hong Kong), and the GIS User Community";

        const cartoAttr =
          "&copy; <a href='http://www.openstreetmap.org/copyright'>OpenStreetMap</a> " +
          "&copy; <a href='http://cartodb.com/attributions'>CartoDB</a>";

        const geodirAttr ="<a href='http://www.geodir.co/'><b>Geodir</b></a> &copy; Map";

        const satelitalAttr = "Kenyojoel903@gmail.com";

        this.mapasBase = {
          Stadia_AlidadeSmoothDark : L.tileLayer('https://tiles.stadiamaps.com/tiles/alidade_smooth_dark/{z}/{x}/{y}{r}.png', {
            maxZoom: 20,
            attribution: darkAttr
          }),
          OpenStreetMap: L.tileLayer(
            "http://{s}.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png",
            {
              attribution: osmAttr
            }
          ),
          Satelital: L.tileLayer(
            "https://wxs.ign.fr/{apikey}/geoportail/wmts?REQUEST=GetTile&SERVICE=WMTS&VERSION=1.0.0&STYLE={style}&TILEMATRIXSET=PM&FORMAT={format}&LAYER=ORTHOIMAGERY.ORTHOPHOTOS&TILEMATRIX={z}&TILEROW={y}&TILECOL={x}",
            {
              attribution: esriAttr,
              bounds: [[-75, -180], [81, 180]],
              minZoom: 2,
              maxZoom: 19,
              apikey: 'choisirgeoportail',
              format: 'image/jpeg',
              style: 'normal'
            },
          ),
          CartoDB: L.tileLayer(
            "http://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}.png",
            {
              attribution: cartoAttr
            }
          ),
        };
  }
}
