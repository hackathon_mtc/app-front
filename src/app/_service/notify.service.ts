import { Injectable } from '@angular/core';

import { environment } from '../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class NotifyService {

  private host: string = environment.url_base;
  private _path:string = 'api/notify/';

  constructor(
    private http: HttpClient

  ) { }

  private get _getOptions() {
    return {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        //'Authorization': `Bearer ${localStorage.getItem('token') ? localStorage.getItem('token') : ''}`
      })
    }
  }

  public getAlerta(id:number){
    return this.http.get(`${this.host}${this._path}${id}`, this._getOptions).pipe(
      map(res => {return res})
    );
  }

  public getComisarias(id:number){
    return this.http.get(`${this.host}${this._path}comisarias/${id}`, this._getOptions).pipe(
      map(res => {return res})
    );
  }

  public getBomberos(id:number){
    return this.http.get(`${this.host}${this._path}bomberos/${id}`, this._getOptions).pipe(
      map(res => {return res})
    );
  }

  public getHospitales(id:number){
    return this.http.get(`${this.host}${this._path}hospitales/${id}`, this._getOptions).pipe(
      map(res => {return res})
    );
  }
}
